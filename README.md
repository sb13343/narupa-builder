![Narupa Builder Logo](/Assets/Splash.png)

This repository contains the Narupa Builder, an interactive virtual reality tool designed to create and manipulate molecules.

This repository is maintained by the Intangible Realities Laboratory, University Of Bristol, 
and distributed under [GPLv3](LICENSE).
See [the list of contributors](CONTRIBUTORS.md) for the individual authors of the project.

## Getting Started

The builder is designed to support the following headsets using the SteamVR framework:

*  HTC Vive
*  Oculus Rift
*  Valve Index

Other VR controllers can be configured to work with Narupa Builder by setting up a custom input binding within Steam, however this is unsupported by the IRL team.

This project currently uses Unity 2019.2

## OpenBabel

Minimisation and advanced import/export requires OpenBabel *3.0+* to be installed. This can be obtained here: https://github.com/openbabel/openbabel/releases/latest.

## Credits and External Libraries

This project has been made possible by the following projects. We gratefully thank them for their efforts, and suggest that you use and cite them:

* [Narupa](https://gitlab.com/intangiblerealities/narupa-applications/narupa-imd) (GPLv3) - Framework for interactive molecular simulation in VR.
* [SteamVR SDK](https://github.com/ValveSoftware/steamvr_unity_plugin) (BSD) - SDK for developing VR applications in SteamVR.
* [OpenBabel 3.0](https://github.com/openbabel/openbabel/releases/latest) (GPLv2) - Chemical file format handling and minimisation.
