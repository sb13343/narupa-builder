// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using UnityEngine;
using UnityEngine.Events;

namespace NarupaBuilder.Tools
{
    /// <summary>
    /// General implementation of a tool as a <see cref="ScriptableObject" />. The
    /// <see cref="startAction" /> field can be used to add a callback to when a Tool
    /// is requested to prepare its action.
    /// </summary>
    [CreateAssetMenu(fileName = "New Tool", menuName = "Tool")]
    public class Tool : ScriptableObjectTool
    {
        [SerializeField]
        private string displayName;

        [SerializeField]
        private Sprite image;

        [SerializeField]
        private GameObject toolEnd;

        /// <inheritdoc cref="ScriptableObjectTool.DisplayName" />
        public override string DisplayName => displayName;

        /// <inheritdoc cref="ScriptableObjectTool.SpriteIcon" />
        public override Sprite SpriteIcon => image;

        /// <inheritdoc cref="ScriptableObjectTool.ControllerGizmo" />
        public override GameObject ControllerGizmo => toolEnd;

        /// <inheritdoc cref="ScriptableObjectTool.StartAction" />
        public override void StartAction()
        {
            startAction?.Invoke();
        }

        [SerializeField]
        private StartActionEvent startAction;

        [Serializable]
        public class StartActionEvent : UnityEvent
        {
        }
    }
}