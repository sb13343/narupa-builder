// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace NarupaBuilder.Tools
{
    public enum SelectModes
    {
        SelectSingle,
        SelectFragment
    }
}