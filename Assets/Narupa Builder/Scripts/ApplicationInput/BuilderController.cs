﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.Frontend.Controllers;
using Narupa.Frontend.Input;
using NarupaBuilder.Tools;

namespace NarupaBuilder
{
    public class BuilderController
    {
        public IPosedObject ToolPose { get; set; }
        public IButton InteractAction { get; set; }
        public VrController Controller { get; set; }

        public ToolRunner ToolRunner { get; set; }

        public Sphere ToolSphere => ToolRunner.GetWorldSpaceToolSphere();
    }
}