﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using System.Linq;
using Narupa.Core.Math;
using UnityEngine;

namespace NarupaBuilder
{
    ///<summary> 
    /// Class that defines an object that connects to other Particles, by a bond
    ///</summary>
    public abstract class Particle
    {
        protected bool Equals(Particle other)
        {
            return Position == other.Position && CurrentIndex == other.CurrentIndex;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Particle) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Pose.GetHashCode() * 397) ^ (int) CurrentIndex;
            }
        }

        public abstract float Scale { get; }
        public uint CurrentIndex { get; set; }
        public List<Bond> MyBonds { get; set; } = new List<Bond>();
        public int TotalBondOrder => (MyBonds.Select(b => b.BondOrder)).Sum();

        ///<summary>
        /// List of Particles bonded to this Particle.
        ///</summary>
        public List<Particle> BondedParticles { get; set; } = new List<Particle>();

        public PointTransformation Pose;
        public bool ScaledDown { get; set; }
        public abstract int CommonBondNumber { get; }

        public Vector3 Position
        {
            get => Pose.Position;
            set => Pose.Position = value;
        }

        public Particle(uint currentIndex, PointTransformation pose, bool scaledDown = false)
        {
            CurrentIndex = currentIndex;
            Pose = pose;
            ScaledDown = scaledDown;
        }

        public abstract Particle Copy(bool scaledDown = false, uint? currentIndex = null);
    }
}