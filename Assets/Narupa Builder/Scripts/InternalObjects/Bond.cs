﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NarupaBuilder
{
    ///<summary> 
    /// Class that defines an object that connects Particles (ie this is a bond). 
    ///</summary>
    public class Bond
    {
        public uint CurrentIndex { get; set; }

        public int BondOrder { get; set; }

        public Particle A { get; private set; }

        public Particle B { get; private set; }

        public IEnumerable<Particle> BondedParticles
        {
            get
            {
                yield return A;
                yield return B;
            }
        }

        public IEnumerable<Atom> BondedAtoms => BondedParticles.OfType<Atom>();

        public Bond(uint currentIndex, Particle a, Particle b, int bondOrder = 1)
        {
            CurrentIndex = currentIndex;
            A = a;
            B = b;
            BondOrder = bondOrder;
        }

        public bool IdenticalBondExistsInList(List<Bond> bonds)
        {
            bool contained = false;
            foreach (Bond b in bonds)
            {
                if (A == b.A && B == b.B)
                {
                    contained = true;
                }

                if (A == b.B && B == b.A)
                {
                    contained = true;
                }
            }

            return contained;
        }


        public bool IdenticalBondByPositionExistsInList(List<Bond> bonds)
        {
            bool contained = false;
            foreach (Bond b in bonds)
            {
                if (b.A.Pose.Position == A.Pose.Position && B.Pose.Position == B.Pose.Position)
                {
                    contained = true;
                }

                if (b.B.Pose.Position == A.Pose.Position && b.A.Pose.Position == B.Pose.Position)
                {
                    contained = true;
                }
            }

            return contained;
        }

        public bool IdenticalBondByIndexExistsInList(IEnumerable<Bond> bonds)
        {
           foreach (var b in bonds)
            {
                if (b.A.CurrentIndex == A.CurrentIndex && b.B.CurrentIndex == B.CurrentIndex)
                {
                    return true;
                }
                if (b.A.CurrentIndex == B.CurrentIndex && b.B.CurrentIndex == A.CurrentIndex)
                {
                    return true;
                }
            }

           return false;
        }

        public bool SharesBondedParticle(Bond b)
        {
            if (b.A == A || b.B == B || b.A == B || b.B == A)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public Vector3 GetBondDirectionFromAToB()
        {
            return (B.Pose.Position - A.Pose.Position).normalized;
        }

        public void PrintBondedParticleIndexes()
        {
            Debug.Log("This bond is bonded to the particles :" + A.CurrentIndex + " and " + B.CurrentIndex);
        }

        public Particle Other(Particle particle)
        {
            if (A.Equals(particle))
                return B;
            if (B.Equals(particle))
                return A;
            throw new InvalidOperationException();
        }

        public Particle this[int i]
        {
            get
            {
                if (i == 0)
                    return A;
                if (i == 1)
                    return B;
                throw new InvalidOperationException();
            }
            set
            {
                if (i == 0)
                    A = value;
                else if (i == 1)
                    B = value;
                else
                    throw new InvalidOperationException();
            }
        }

        public bool ContainsAny(IEnumerable<Particle> particles)
        {
            foreach (var particle in particles)
            {
                if (A.Equals(particle))
                    return true;
                if (B.Equals(particle))
                    return true;
            }
            return false;
        }
    }
}