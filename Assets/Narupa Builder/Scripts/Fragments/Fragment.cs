namespace NarupaBuilder
{
    /// <summary>
    /// Represents a molecular system with a given name.
    /// </summary>
    public class Fragment
    {
        /// <summary>
        /// The name of the fragment.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// The molecular system within this fragment.
        /// </summary>
        public BondsAndParticles System { get; }

        public Fragment(string name, BondsAndParticles system)
        {
            Name = name;
            System = system;
        }
    }
}