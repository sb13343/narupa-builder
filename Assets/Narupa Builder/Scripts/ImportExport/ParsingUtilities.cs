using System.Globalization;

namespace NarupaBuilder
{
    /// <summary>
    /// Utility methods for parsing from strings.
    /// </summary>
    public static class ParsingUtilities
    {
        /// <summary>
        /// Parses an integer from a substring, using the given culture.
        /// </summary>
        /// <param name="str"> String to parse from. </param>
        /// <param name="start"> Starting index. </param>
        /// <param name="length"> Length of substring. </param>
        /// <param name="culture"> Culture to use to parse the integer, defaults to InvariantCulture. </param>
        /// <returns>Integer parsed from the substring. </returns>
        public static int ParseIntFromSubstring(string str, int start, int length, CultureInfo culture=null)
        {
            if (culture == null)
                culture = CultureInfo.InvariantCulture;
            return int.Parse(str.Substring(start, length), culture);
        }

        /// <summary>
        /// Parses a float from a substring, using the given culture.
        /// </summary>
        /// <param name="str"> String to parse from. </param>
        /// <param name="start"> Starting index. </param>
        /// <param name="length"> Length of substring. </param>
        /// <param name="culture"> Culture to use to parse the integer, defaults to InvariantCulture. </param>
        /// /// <returns>Integer parsed from the substring. </returns>
        public static float ParseFloatFromSubstring(string str, int start, int length, CultureInfo culture=null)
        {
            if (culture == null)
                culture = CultureInfo.InvariantCulture;
            return float.Parse(str.Substring(start, length), culture);
        }
    }
}