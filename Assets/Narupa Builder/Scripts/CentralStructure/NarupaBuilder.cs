﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.Frontend.Manipulation;
using UnityEngine;

namespace NarupaBuilder
{
    /// <summary>
    /// The entry point to the application, and central location for accessing
    /// shared resources.
    /// </summary>
    [DisallowMultipleComponent]
    public class NarupaBuilder : MonoBehaviour
    {
#pragma warning disable 0649

        [SerializeField]
        public Transform simulationSpaceTransform;

#pragma warning restore 0649
        private StateData stateData = new StateData();

        /// <summary>
        /// The route through which simulation space can be manipulated with
        /// gestures to perform translation, rotation, and scaling.
        /// </summary>
        public ManipulableTransform ManipulableSimulationSpace { get; private set; }

        public BondsAndParticles EditableActiveFrame { get; set; } = new BondsAndParticles();

        public void ExecuteUndo()
        {
            stateData.ExecuteUndo();
            SetDataToCurrentState();
        }

        public void ExecuteRedo()
        {
            stateData.ExecuteRedo();
            SetDataToCurrentState();
        }

        private bool isSystemDirty = true;

        public void SetSystemDirty()
        {
            isSystemDirty = true;
        }

        public void SaveFrameDataToUndo()
        {
            if (isSystemDirty)
            {
                stateData.AddFrameAsLastInHistory(EditableActiveFrame);
                FrameChanged?.Invoke();
                isSystemDirty = false;
            }
        }

        private void SetDataToCurrentState()
        {
            EditableActiveFrame.Clear();
            EditableActiveFrame = BondsAndParticles.DeepCopy(stateData.CurrentState);
            FrameChanged?.Invoke();
        }

        private void Awake()
        {
            ManipulableSimulationSpace = new ManipulableTransform(simulationSpaceTransform);
            SaveFrameDataToUndo();
        }

        public event Action FrameChanged;
    }
}