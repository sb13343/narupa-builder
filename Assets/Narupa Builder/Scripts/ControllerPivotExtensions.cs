using Narupa.Frontend.Controllers;
using Narupa.Frontend.Input;
using NarupaBuilder;

namespace Narupa_Builder.Scripts
{
    public static class PosedObjectExtensions
    {
        public static Sphere AsWorldSpaceSphere(this IPosedObject pivot)
        {
            return new Sphere(pivot.Pose.Value.Position, pivot.Pose.Value.Scale.x);
        }
    }
}