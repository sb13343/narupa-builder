﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Linq;
using Narupa.Core.Math;
using Narupa.Core.Science;
using UnityEngine;

namespace NarupaBuilder
{
    public static class ExtensionMethods
    {
        public static Transformation WithPosition(this Transformation trans, Vector3 position)
        {
            trans.Position = position;
            return trans;
        }

        public static Transformation AddPosition(this Transformation trans, Vector3 position)
        {
            trans.Position += position;
            return trans;
        }
        
        public static Transformation InSimulationSpace(this Transformation trans,
                                                       Transform localSpace)
        {
            var transformed = localSpace.worldToLocalMatrix * trans.Matrix;
            if(!transformed.ValidTRS())
                throw new InvalidOperationException("Transformation failed!");
            return new Transformation(transformed.GetTranslation(), transformed.rotation,
                                      transformed.lossyScale);
        }

        public static List<Particle> DeepCopyParticleListWithoutConnections(
            this List<Particle> particles)
        {
            return particles.Select(p => p.Copy()).ToList();
        }

        public static List<Bond> DeepCopyBondList(this List<Bond> bonds)
        {
            var newBonds = new List<Bond>();
            foreach (Bond b in bonds)
            {
                newBonds.Add(new Bond(b.CurrentIndex, b.A, b.B,
                                      b.BondOrder));
            }

            return newBonds;
        }


        public static IEnumerable<Bond> BondsContainingAnyParticles(
            this IEnumerable<Bond> checkBonds,
            IEnumerable<Particle> particles)
        {
            return checkBonds.Where(bond => bond.ContainsAny(particles));
        }


        public static IEnumerable<Bond> BondsContainingAnyParticlesByIndex(
            this IEnumerable<Bond> checkBonds,
            IReadOnlyCollection<Particle> particles)
        {
            foreach (var b in checkBonds)
            {
                foreach (var p in particles)
                {
                    if (b.A.CurrentIndex == p.CurrentIndex ||
                        b.B.CurrentIndex == p.CurrentIndex)
                    {
                        yield return b;
                        break;
                    }
                }
            }
        }

        public static List<Bond> BondsContainingTwoParticles(this IEnumerable<Bond> checkBonds,
                                                             IEnumerable<Particle> particles)
        {
            //this function works!
            return checkBonds.Where(bond => particles.Intersect(bond.BondedParticles).Count() == 2)
                             .ToList();
        }

        public static IEnumerable<Particle> IntersectByIndex(
            this IEnumerable<Particle> a,
            IEnumerable<Particle> b)
        {
            foreach (var particle in a)
            {
                if (b.Any(particle2 => particle.CurrentIndex == particle2.CurrentIndex))
                {
                    yield return particle;
                }
            }
        }

        public static IEnumerable<Bond> IntersectByParticleIndex(
            this IEnumerable<Bond> a,
            IEnumerable<Bond> b)
        {
            foreach (var bond in a)
            {
                if (bond.IdenticalBondByIndexExistsInList(b))
                {
                    yield return bond;
                }
            }
        }

        public static List<Bond> BondsContainingTwoParticlesByPosition(this List<Bond> checkBonds,
                                                                       List<Particle> particles)
        {
            var hits = new List<Bond>();
            foreach (Bond b in checkBonds)
            {
                int num = 0;
                foreach (Particle p in particles)
                {
                    if (p.Pose.Position == b.A.Pose.Position ||
                        p.Pose.Position == b.B.Pose.Position)
                    {
                        num++;
                    }
                }

                if (num == 2)
                {
                    hits.Add(b);
                }
            }

            return hits.Distinct().ToList();
        }

        public static List<Bond> BondsContainingTwoParticlesByCurrentIndex(
            this List<Bond> checkBonds,
            List<Particle> particles)
        {
            var hits = new List<Bond>();
            foreach (Bond b in checkBonds)
            {
                int num = 0;
                foreach (Particle p in particles)
                {
                    if (p.CurrentIndex == b.A.CurrentIndex ||
                        p.CurrentIndex == b.B.CurrentIndex)
                    {
                        num++;
                    }
                }

                if (num == 2)
                {
                    hits.Add(b);
                }
            }

            return hits.Distinct().ToList();
        }

        public static List<Particle> TransformParticles(this IEnumerable<Particle> particles,
                                                        Transformation startTransformation,
                                                        Transformation endTransformation)
        {
            return particles.TransformParticles(endTransformation.Matrix *
                                                startTransformation.Matrix.inverse);
        }

        public static List<Particle> TransformParticles(this IEnumerable<Particle> particles,
                                                        Matrix4x4 matrix)
        {
            var changedParticles = new List<Particle>();
            foreach (var p in particles)
            {
                var newParticle = p.Copy();
                newParticle.Pose = matrix * p.Pose;
                changedParticles.Add(newParticle);
            }

            return changedParticles;
        }

        public static PointTransformation AsPointTransformationWithUnitScale(
            this Transformation transformation)
        {
            return new PointTransformation(transformation.Position);
        }

        public static List<Particle> GetFragmentFromParticle(this Particle particle,
                                                             int particleCount) //where else this used?
        {
            var residueParticlesHash = new HashSet<Particle>(new Particle[]
            {
                particle
            });
            var lastTest = new List<Particle>(new Particle[]
            {
                particle
            });
            for (int i = 0; i < particleCount * 2; i++)
            {
                List<Particle> nextTest = new List<Particle>();
                nextTest = lastTest.SelectMany(p => p.BondedParticles).ToList();
                nextTest = nextTest.Except(residueParticlesHash).ToList();
                if (residueParticlesHash.Intersect(nextTest).Count() != 0)
                {
                    throw new InvalidOperationException(
                        "there should never be a duplicate being added to this list");
                }

                residueParticlesHash.UnionWith(nextTest);
                lastTest.Clear();
                lastTest.AddRange(nextTest);
                if (lastTest.Count == 0)
                {
                    continue;
                }
            }

            return residueParticlesHash.ToList();
        }

        public static List<Bond> ReplaceBondedWithSameIndexParticles(this List<Bond> bonds,
                                                                     List<Particle> newParticles)
        {
            var newBonds = bonds.DeepCopyBondList();
            for (int i = 0; i < bonds.Count; i++)
            {
                for (int k = 0; k < 2; k++)
                {
                    foreach (Particle nP in newParticles)
                    {
                        if (bonds[i][k].CurrentIndex == nP.CurrentIndex)
                        {
                            newBonds[i][k] = nP;
                        }
                    }
                }
            }

            return newBonds;
        }


        public static void ChangeParticleCurrentIndex(this List<Particle> particles, uint newIndex)
        {
            foreach (Particle p in particles)
            {
                p.CurrentIndex = newIndex;
            }
        }

        public static void ChangeBondCurrentIndex(this List<Bond> bonds, uint newIndex)
        {
            foreach (Bond b in bonds)
            {
                b.CurrentIndex = newIndex;
            }
        }

        public static Bond GetIdenticalBondInList(this Bond thisBond, List<Bond> bonds)
        {
            foreach (Bond b in bonds)
            {
                if (b.A.CurrentIndex == thisBond.A.CurrentIndex &&
                    b.B.CurrentIndex == thisBond.B.CurrentIndex)
                {
                    return b;
                }

                if (b.B.CurrentIndex == thisBond.A.CurrentIndex &&
                    b.A.CurrentIndex == thisBond.B.CurrentIndex)
                {
                    return b;
                }
            }

            return null;
        }

        public static List<Particle> GetMatchingParticlesOnListByPosition(this List<Particle> these,
                                                                          List<Particle> toCheck)
        {
            var matching = new List<Particle>();
            foreach (Particle p in these)
            {
                foreach (Particle tC in toCheck)
                {
                    if (p.Pose.Position == tC.Pose.Position)
                    {
                        matching.Add(tC);
                    }
                }
            }

            return matching;
        }

        public static void SetMatchingParticleScaledDownValues(this List<Particle> these,
                                                               List<Particle> toCheck)
        {
            foreach (Particle p in these)
            {
                foreach (Particle tC in toCheck)
                {
                    if (p.Pose.Position == tC.Pose.Position)
                    {
                        p.ScaledDown = tC.ScaledDown;
                    }
                }
            }
        }


        public static List<T> AsList<T>(this T t)
        {
            return new List<T>(new T[]
            {
                t
            });
        }

        public static IEnumerable<T> AsEnumerable<T>(this T t)
        {
            yield return t;
        }

        public static IEnumerable<TAtom> ExceptHydrogens<TAtom>(this IEnumerable<TAtom> atoms)
            where TAtom : Atom
        {
            return atoms.Where(atom => atom.Element != Element.Hydrogen);
        }

        public static IEnumerable<TAtom> Hydrogens<TAtom>(this IEnumerable<TAtom> atoms)
            where TAtom : Atom
        {
            return atoms.Where(atom => atom.Element == Element.Hydrogen);
        }
    }
}