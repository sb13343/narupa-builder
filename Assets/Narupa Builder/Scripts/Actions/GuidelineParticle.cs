// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.Core.Math;
using UnityEngine;

namespace NarupaBuilder
{
    public class GuidelineParticle : Particle
    {
        public GuidelineParticle(uint currentIndex, Vector3 pose, bool scaledDown = false) :
            base(currentIndex, new PointTransformation(pose, 1f), scaledDown)
        {
        }

        public override float Scale => 1f;
        public override int CommonBondNumber => throw new NotImplementedException();

        public override Particle Copy(bool scaledDown = false, uint? currentIndex = null)
        {
            return new GuidelineParticle(currentIndex ?? CurrentIndex, Pose.Position,
                                         scaledDown || ScaledDown);
        }
    }
}