﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using System.Linq;
using Narupa.Core.Math;
using Narupa.Core.Science;
using UnityEngine;

namespace NarupaBuilder
{
    public class BuildFragmentAction : BaseAction
    {
        private Transformation midTrans;

        BondsAndParticles fragmentSystem = new BondsAndParticles();
        private BondsAndParticles fragment;

        private IBuilderVisualiser ghost;

        public static Fragment CurrentFragment { get; set; }

        private List<Merge> merges = new List<Merge>();

        private List<FrameFragmentPair> ffPairs = new List<FrameFragmentPair>();

        public BuildFragmentAction(ActionsManager aM, VisualisationManager vM, Fragment fragment)
            : base(aM, vM)
        {
            CurrentFragment = fragment ?? GetDefaultFragment();
        }

        private static Fragment GetDefaultFragment()
        {
            return new FragmentFileReference($"{Application.streamingAssetsPath}/RGroups/Methyl.mol2")
                   .GetFragmentAsync().GetAwaiter().GetResult();
        }

        public override void OnActionPrepared()
        {
            base.OnActionPrepared();
            ghost = Visualisations.FragmentGhost;

            fragment = BondsAndParticles.DeepCopy(CurrentFragment.System);
            fragment.ReindexBondsAndParticles();
            fragment.UpdateBondedParticlesForParticles();

            // Recenter around zero, then transform to the start pose.
            fragment.RecenterParticles(Vector3.zero);

            UpdateFragmentFromCurrentPose();
        }

        private void UpdateFragmentFromCurrentPose()
        {
            fragmentSystem.Particles =
                fragment.Particles.TransformParticles(SimulationSpacePoseUnitScale.Matrix);
            fragmentSystem.Bonds =
                fragment.Bonds.ReplaceBondedWithSameIndexParticles(
                    fragmentSystem.Particles);
            fragmentSystem.UpdateBondedParticlesForParticles();
        }

        public override void UpdateActionPrepared()
        {
            base.UpdateActionPrepared();
            UpdateFragmentFromCurrentPose();

            ComputeIntersection();
        }

        public override void UpdateActionInProgress()
        {
            base.UpdateActionInProgress();
            UpdateFragmentFromCurrentPose();
        }

        public override void OnActionFinished()
        {
            base.OnActionFinished();
            PerformAction();
            ClearUp();
        }

        public override void OnActionCancelled()
        {
            base.OnActionCancelled();
            ClearUp();
        }

        private void PerformAction()
        {
            var molecule = RenderMerge(CurrentSystem, true);
            CurrentSystem.Particles.AddRange(molecule.Atoms);
            CurrentSystem.Bonds.AddRange(molecule.Bonds);

            CurrentSystem.ReindexBondsAndParticles();
            CurrentSystem.UpdateBondedParticlesForParticles();
            Actions.SetSystemDirty();
        }

        private void ClearUp()
        {
            ghost.Destroy();
        }

        public override void RenderActionPrepared(BondsAndParticles currentSystem,
                                                  BondsAndParticles currentSelection)
        {
            base.RenderActionPrepared(currentSystem, currentSelection);
            RenderAction(currentSystem);
        }

        private BondsAndParticles RenderMerge(BondsAndParticles currentSystem, bool linkSystems)
        {
            ffPairs.Clear();
            var molecule = BondsAndParticles.DeepCopy(fragmentSystem);
            var newBonds = new List<Bond>();
            foreach (var m in merges)
            {
                switch (m.Type)
                {
                    case MergeType.MergeNonHydrogenOnFragOntoHydrogenFrame:
                        foreach (var dP in m.DestinationParents)
                        {
                            Atom dAtomParent = currentSystem.GetParticleWithSameIndex(dP) as Atom;
                            var forGhost = new Atom(dAtomParent.Element,
                                                    (uint) molecule.Particles.Count + 2,
                                                    dAtomParent.Pose);
                            Bond newBond = null;
                            if (linkSystems)
                            {
                                newBond = new Bond(
                                    (uint) (molecule.Bonds.Count + currentSystem.Bonds.Count + 2),
                                    dAtomParent, molecule.GetParticleWithSameIndex(m.Source));
                            }
                            else
                            {
                                molecule.Particles.Add(forGhost);
                                newBond = new Bond((uint) molecule.Bonds.Count + 2, forGhost,
                                                   molecule.GetParticleWithSameIndex(m.Source));
                            }

                            newBonds.Add(newBond);
                            ffPairs.Add(new FrameFragmentPair(
                                            dAtomParent,
                                            molecule.GetParticleWithSameIndex(m.Source) as Atom,
                                            forGhost, newBond, m.Type));
                        }

                        currentSystem.RemoveParticlesAndRelatedBondsByIndex(m.Destination.AsList());
                        break;
                    case MergeType.MergeHydrogenOnFragIntoNonHydrogenFrame:

                        Atom destAtom = m.Destination;

                        var forGhostDest = new Atom(destAtom.Element,
                                                    (uint) molecule.Particles.Count + 2,
                                                    destAtom.Pose);
                        if (!linkSystems)
                        {
                            molecule.Particles.Add(forGhostDest);
                        }

                        foreach (var sP in m.SourceParents)
                        {
                            Atom sAtomParent = molecule.GetParticleWithSameIndex(sP) as Atom;
                            Bond newBond = null;
                            if (linkSystems)
                            {
                                newBond = new Bond(
                                    (uint) (molecule.Bonds.Count + currentSystem.Bonds.Count + 2),
                                    destAtom, sAtomParent);
                            }
                            else
                            {
                                newBond = new Bond((uint) molecule.Bonds.Count + 2, forGhostDest,
                                                   sAtomParent);
                            }

                            newBonds.Add(newBond);
                            ffPairs.Add(new FrameFragmentPair(
                                            destAtom, molecule.GetParticleWithSameIndex(sP) as Atom,
                                            forGhostDest, newBond, m.Type));
                        }


                        molecule.RemoveParticlesAndRelatedBondsByIndex(m.Source.AsList());

                        break;

                    case MergeType.MergeHydrogenIntoHydrogen:
                        foreach (var dP in m.DestinationParents)
                        {
                            Atom dAtomParent = currentSystem.GetParticleWithSameIndex(dP) as Atom;
                            var forGhostDestHydHyd =
                                new Atom(dAtomParent.Element, (uint) molecule.Particles.Count + 2,
                                         dAtomParent.Pose);
                            if (!linkSystems)
                            {
                                molecule.Particles.Add((forGhostDestHydHyd));
                            }

                            foreach (var sP in m.SourceParents)
                            {
                                Atom sAtomParent = molecule.GetParticleWithSameIndex(sP) as Atom;
                                Bond newBond = null;
                                if (linkSystems)
                                {
                                    newBond = new Bond(
                                        (uint) (molecule.Bonds.Count + currentSystem.Bonds.Count +
                                                2), dAtomParent, sAtomParent);
                                }
                                else
                                {
                                    newBond = new Bond((uint) molecule.Bonds.Count + 2,
                                                       forGhostDestHydHyd,
                                                       sAtomParent);
                                }

                                newBonds.Add(newBond);
                                ffPairs.Add(new FrameFragmentPair(
                                                dP, molecule.GetParticleWithSameIndex(sP) as Atom,
                                                forGhostDestHydHyd, newBond, m.Type));
                            }
                        }

                        currentSystem.RemoveParticlesAndRelatedBondsByIndex(m.Destination.AsList());

                        molecule.RemoveParticlesAndRelatedBondsByIndex(m.Source.AsList());

                        break;
                }
            }

            molecule.Bonds.AddRange(newBonds);
            molecule.ReindexBondsAndParticles();
            currentSystem.ReindexBondsAndParticles();

            molecule.UpdateBondedParticlesForParticles();
            currentSystem.UpdateBondedParticlesForParticles();

            if (merges.Count != 0)
            {
                FixDoubleBondsAndRemovals(molecule, currentSystem, linkSystems);
            }

            return molecule;
        }

        private void FixDoubleBondsAndRemovals(BondsAndParticles molecule,
                                               BondsAndParticles currentSystem,
                                               bool linkSystems)
        {
            foreach (var ffPair in ffPairs)
            {
                var otherInFrames = new List<Particle>();
                var otherInFragments = new List<Particle>();
                foreach (var otherff in ffPairs)
                {
                    if (otherff != ffPair)
                    {
                        otherInFrames.Add(otherff.InFrame);
                        otherInFragments.Add(otherff.InFragment);
                    }
                }

                switch (ffPair.Type)
                {
                    case MergeType.MergeHydrogenOnFragIntoNonHydrogenFrame:
                        if (!otherInFrames.Contains(ffPair.InFrame))
                        {
                            RemoveHydrogensIfRelevant(ffPair.InFrame, currentSystem,
                                                      ffPair.InFragment.Pose.Position);
                        }

                        break;
                    case MergeType.MergeNonHydrogenOnFragOntoHydrogenFrame:
                        if (!otherInFragments.Contains(ffPair.InFragment))
                        {
                            if (linkSystems)
                            {
                                RemoveHydrogensIfRelevant(ffPair.InFragment, molecule,
                                                          ffPair.InFrame.Pose.Position, -1);
                            }
                            else
                            {
                                RemoveHydrogensIfRelevant(ffPair.InFragment, molecule,
                                                          ffPair.InFrame.Pose.Position, -1);
                            }
                        }

                        break;
                }
            }

            foreach (var ffPair in ffPairs)
            {
                foreach (var otherff in ffPairs)
                {
                    if (otherff != ffPair)
                    {
                        if (otherff.InFrame.CurrentIndex == ffPair.InFrame.CurrentIndex &&
                            otherff.InFragment.CurrentIndex == ffPair.InFragment.CurrentIndex &&
                            ffPair.FakeBondBetween.BondOrder != 0)
                        {
                            ffPair.FakeBondBetween.BondOrder++;
                            otherff.FakeBondBetween.BondOrder = 0;
                        }
                    }
                }
            }

            foreach (var ffPair in ffPairs)
            {
                if (ffPair.FakeBondBetween.BondOrder == 0)
                {
                    molecule.RemoveBondsByIndex(ffPair.FakeBondBetween.AsList());
                }
            }

            void RemoveHydrogensIfRelevant(Particle removeFrom,
                                           BondsAndParticles removeWith,
                                           Vector3 otherPosition,
                                           int mod = 0)
            {
                int hydrogensToRemove =
                    (removeFrom.TotalBondOrder + 1 + mod) - removeFrom.CommonBondNumber;
                removeFrom = removeWith.GetParticleWithSamePosition(removeFrom);
                if (hydrogensToRemove > 0)
                {
                    var toRemove = removeFrom
                                   .BondedParticles.Select(p => p as Atom)
                                   .Where(a => a.Element == Element.Hydrogen &&
                                               a.Pose.Position != otherPosition)
                                   .OrderBy(a => Vector3.Distance(a.Pose.Position, otherPosition))
                                   .ToList();
                    removeWith.RemoveParticlesAndRelatedBondsByIndex(
                        toRemove.Take(Mathf.Min(toRemove.Count, hydrogensToRemove)).ToList());
                }
            }
        }

        //        private BondsAndParticles PerformMerge(BondsAndParticles currentSystem,
        //                                               bool linkToEachOther)
        //        {
        //            if (merge != null)
        //            {
        //                var molecule = BondsAndParticles.DeepCopy(fragmentSystem);
        //                var mergeSourceAtom = molecule.GetParticleWithSameIndex(merge.Source);
        //                var mergeDestinationAtom =
        //                    currentSystem.Particles[(int) merge.Destination.CurrentIndex] as Atom;
        //
        //                molecule.RemoveParticlesAndRelatedBondsByIndex(merge.SourceToDelete);
        //                currentSystem.RemoveParticlesAndRelatedBondsByIndex(merge.DestinationToDelete);
        //
        //                switch (merge.Type)
        //                {
        //                    case MergeType.MergeHydrogenOnFragIntoNonHydrogenFrame:
        //                       // mergeSourceAtom.Element = merge.Destination.Element;
        //                        mergeSourceAtom.Pose = merge.Destination.Pose;
        //                        break;
        //                    case MergeType.MergeNonHydrogenOnFragOntoHydrogenFrame:
        //                        // Remove destination hydrogen (and bond)
        //                        // Link source to the destination hydrogen's neighbor
        //                        currentSystem.RemoveParticlesAndRelatedBondsByIndex(
        //                            merge.Destination.AsList());
        //                        Particle destNeighbor = merge.Destination.BondedAtoms.First();
        //                        if (linkToEachOther)
        //                            destNeighbor =
        //                                currentSystem.GetParticleWithSameIndex(destNeighbor);
        //                        else
        //                            destNeighbor = destNeighbor.Copy();
        //                        molecule.Particles.Add(destNeighbor);
        //                        molecule.Bonds.Add(new Bond((uint) molecule.Bonds.Count,
        //                                                    destNeighbor,
        //                                                    mergeSourceAtom));
        //                        break;
        //                    case MergeType.MergeHydrogenIntoHydrogen:
        //                        Particle srcToConnect = merge.Source.BondedAtoms.First();
        //                        srcToConnect = molecule.GetParticleWithSameIndex(srcToConnect);
        //                        Particle destToConnect = merge.Destination.BondedAtoms.First();
        //                        
        //                        molecule.RemoveParticlesAndRelatedBondsByIndex(merge.Source.AsList());
        //                        currentSystem.RemoveParticlesAndRelatedBondsByIndex(
        //                            merge.Destination.AsList());
        //
        //                        if (linkToEachOther)
        //                            destToConnect = currentSystem.GetParticleWithSameIndex(destToConnect);
        //                        else
        //                            destToConnect = destToConnect.Copy();
        //                        molecule.Bonds.Add(new Bond((uint) molecule.Bonds.Count,
        //                                                    srcToConnect,
        //                                                    destToConnect));
        //                        break;
        //                }
        //
        //
        //                molecule.Bonds.ReplaceBondedWithSameIndexParticles(currentSystem.Particles);
        //
        //                return molecule;
        //            }
        //            else
        //            {
        //                return fragmentSystem;
        //            }
        //        }

        private void RenderAction(BondsAndParticles currentSystem)
        {
            ghost.SetFrame(RenderMerge(currentSystem, false));
            // ghost.SetFrame(PerformMerge(currentSystem, false));
        }

        enum MergeType
        {
            Invalid,
            MergeHydrogenIntoHydrogen,
            MergeHydrogenOnFragIntoNonHydrogenFrame,
            MergeNonHydrogenOnFragOntoHydrogenFrame
        }

        public override void RenderActionInProgress(BondsAndParticles currentSystem,
                                                    BondsAndParticles currentSelection)
        {
            base.RenderActionInProgress(currentSystem, currentSelection);
            RenderAction(currentSystem);
        }

        private Merge merge;

        class Merge
        {
            public Atom Source;
            public Atom Destination;
            public MergeType Type;
            public List<Atom> SourceToDelete = new List<Atom>();
            public List<Atom> DestinationToDelete = new List<Atom>();
            public List<Atom> SourceParents = new List<Atom>();
            public List<Atom> DestinationParents = new List<Atom>();
        }

        class FrameFragmentPair
        {
            public Atom InFrame;
            public Atom InFragment;
            public Atom FakeInFrame;
            public Bond FakeBondBetween;
            public MergeType Type;

            public FrameFragmentPair(Atom inFrame,
                                     Atom inFragment,
                                     Atom fakeInFrame,
                                     Bond fakeBondBetween,
                                     MergeType type)
            {
                InFrame = inFrame;
                InFragment = inFragment;
                FakeInFrame = fakeInFrame;
                FakeBondBetween = fakeBondBetween;
                Type = type;
            }
        }


        public void ComputeIntersection()
        {
            var potentialMerges = new List<Merge>();
            foreach (var fragmentAtom in fragmentSystem.Atoms)
            {
                foreach (var existingAtom in CurrentSystem.Atoms)
                {
                    var sqrDist =
                        Vector3.SqrMagnitude(
                            fragmentAtom.Pose.Position - existingAtom.Pose.Position);
                    if (sqrDist > 0.0025f)
                        continue;
                    var type = CanMergeAtoms(fragmentAtom, existingAtom);
                    if (type != MergeType.Invalid)
                    {
                        potentialMerges.Add(new Merge()
                        {
                            Destination = existingAtom,
                            Source = fragmentAtom,
                            Type = type
                        });
                    }
                }
            }


            foreach (Merge m in potentialMerges)
            {
                switch (m.Type)
                {
                    case MergeType.MergeNonHydrogenOnFragOntoHydrogenFrame:
                        m.DestinationParents = m.Destination.BondedAtoms.ToList();

                        break;
                    case MergeType.MergeHydrogenOnFragIntoNonHydrogenFrame:

                        m.SourceParents = m.Source.BondedAtoms.ToList();

                        break;
                    case MergeType.MergeHydrogenIntoHydrogen:
                        m.DestinationParents = m.Destination.BondedAtoms.ToList();
                        m.SourceParents = m.Source.BondedAtoms.ToList();

                        break;
                    default:
                        //do nothing
                        break;
                }
            }

            merges = potentialMerges;
        }

        private MergeType CanMergeAtoms(Atom fragmentAtom, Atom existingAtom)
        {
            if (fragmentAtom.Element == Element.Hydrogen &&
                existingAtom.Element == Element.Hydrogen)
            {
                return MergeType.MergeHydrogenIntoHydrogen;
            }

            if (fragmentAtom.Element == Element.Hydrogen)
            {
                var spareBonds = existingAtom.CommonBondNumber -
                                 existingAtom.BondedAtoms.ExceptHydrogens().Count();
                return MergeType.MergeHydrogenOnFragIntoNonHydrogenFrame;
            }

            if (existingAtom.Element == Element.Hydrogen)
            {
                var spareBonds = fragmentAtom.CommonBondNumber -
                                 fragmentAtom.BondedAtoms.ExceptHydrogens().Count();
                return MergeType.MergeNonHydrogenOnFragOntoHydrogenFrame;
            }
            else
            {
                // TODO: Merging non hydrogens together
                /*
                var toMergeCurrentBonds = toMergeAtom.BondedAtoms.ExceptHydrogens().Count();
                var spareBonds1 = toMergeAtom.CommonBondNumber - toMergeCurrentBonds;
                var targetAtomCurrentBonds = targetAtom.BondedAtoms.ExceptHydrogens().Count();
                var spareBonds2 = targetAtom.CommonBondNumber - targetAtomCurrentBonds;

                return spareBonds1 >= targetAtomCurrentBonds &&
                       spareBonds2 >= targetAtomCurrentBonds;
                       */
            }

            return MergeType.Invalid;
        }
    }
}