﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using System.Linq;
using Narupa.Core.Math;
using Narupa.Core.Science;
using UnityEngine;

namespace NarupaBuilder
{
    public class BuildAction : BaseAction
    {
        private Element element;

        public BuildAction(ActionsManager aM, VisualisationManager vM, Element element) :
            base(aM, vM)
        {
            this.element = element;
        }

        ///I imagine these two functions could be cleaned up one way or another, defs could do some linq magic on some of these
        private List<GuidelineData> GetGuidelinesAndDoSnap(IEnumerable<Atom> cores,
                                                           Vector3 pose)
        {
            var newParticlePositionMod = Vector3.zero;
            var guidelineClusters = Guidelines.CalculateGuidelines(cores, pose);
            foreach (var gD in guidelineClusters)
            {
                var idealBondLength =
                    ElementValues.GetIdealBondLength(gD.core, newAtom, 1);

                gD.positions = gD.positions.Select(v => v * idealBondLength).ToList();

                var guideParticles =
                    gD.positions.Select(v => new GuidelineParticle(
                                            0,
                                            v + gD.core.Pose.Position)).ToList();

                var closeGuides = HitChecks.SortedParticlesToPoint(pose, guideParticles);
                var closestGuide = closeGuides.FirstOrDefault();
                if (closestGuide != null)
                {
                    newParticlePositionMod +=
                        (closestGuide.Pose.Position - newAtom.Pose.Position) /
                        Mathf.Pow(
                            (1f + 10f * (closestGuide.Pose.Position - newAtom.Pose.Position)
                             .magnitude), 2f);
                }
            }

            newAtom.Pose.Position += newParticlePositionMod;
            return guidelineClusters;
        }

        private IBuilderVisualiser ghostVisualisation;
        private GuidelinesVisualiser guidelinesVisualiserValid;

        private GuidelinesVisualiser guidelinesVisualiserInvalid;


        public override void OnActionPrepared()
        {
            base.OnActionPrepared();
            ghostVisualisation = Visualisations.BuildGhost;
            guidelinesVisualiserValid = Visualisations.BuildGuidelinesValid;
            guidelinesVisualiserInvalid = Visualisations.BuildGuidelinesInvalid;

            UpdateAction();
        }

        enum BuildActionType
        {
            ReplaceParticles,
            AddParticle,
        }

        private Atom newAtom;

        private List<Particle> particlesToAlter;

        private BuildActionType type;

        private List<GuidelineData> guidelines = new List<GuidelineData>();

        private void UpdateAction()
        {
            newAtom = new Atom(element, 0, SimulationSpacePose.AsPointTransformationWithUnitScale());

            var particlesToReplace = GetHighlightedParticlesInOrderForBuildReplace();
            var particlesToBondTo =
                HitChecks.BiasedSortedParticlesBondOverlap(
                    newAtom, CurrentSystem.Particles);
            guidelines.Clear();

            particlesToAlter = particlesToReplace.ToList();
            if (particlesToAlter.Count > 0)
            {
                type = BuildActionType.ReplaceParticles;
                newAtom = null;
            }
            else
            {
                type = BuildActionType.AddParticle;
                if (particlesToBondTo.Count > 0)
                    guidelines =
                        GetGuidelinesAndDoSnap(particlesToBondTo.OfType<Atom>(),
                                               SimulationSpacePose.Position);
                particlesToAlter = particlesToBondTo;
            }
        }

        private void RenderAction(BondsAndParticles currentMolecule)
        {
            switch (type)
            {
                case BuildActionType.AddParticle:
                    if (particlesToAlter.Count == 0)
                    {
                        ghostVisualisation.SetBondsAndParticles(new List<Particle>() { newAtom },
                                                                new List<Bond>());
                    }
                    else
                    {
                        var particlesToDraw = new List<Particle>();
                        particlesToDraw.Add(newAtom);
                        particlesToDraw.AddRange(particlesToAlter);
                        var bondsToCreate =
                            particlesToAlter.Select(p => new Bond(0, p, newAtom)).ToList();
                        ghostVisualisation.SetBondsAndParticles(particlesToDraw,
                                                                bondsToCreate);
                    }

                    break;
                case BuildActionType.ReplaceParticles:
                    foreach (var particleToReplace in particlesToAlter)
                    {
                        var atom =
                            currentMolecule.Particles[(int) particleToReplace.CurrentIndex] as Atom;
                        atom.Element = this.element;
                    }

                    ghostVisualisation.ClearFrame();
                    break;
            }

            //guidelinesVisualiserValid.SetGuidelines(guidelines);
            if (type == BuildActionType.AddParticle && particlesToAlter.Count > 0)
            {
                var invalidGuidelines = new List<GuidelineData>();
                var validGuidelines = new List<GuidelineData>();

                foreach (Particle particleInFrame in particlesToAlter)
                {
                    foreach (GuidelineData gD in guidelines)
                    {
                        if (particleInFrame.Pose.Position == gD.core.Pose.Position)
                        {
                            if (particleInFrame.CommonBondNumber <= particleInFrame.TotalBondOrder)
                            {
                                invalidGuidelines.Add(gD);
                            }
                            else
                            {
                                validGuidelines.Add(gD);
                            }
                        }
                    }
                }

                guidelinesVisualiserValid.SetGuidelines(validGuidelines);
                guidelinesVisualiserInvalid.SetGuidelines(invalidGuidelines);
            }
            else
            {
                guidelinesVisualiserValid.SetGuidelines(guidelines);
                guidelinesVisualiserInvalid.SetGuidelines(new List<GuidelineData>());
            }
        }

        public override void RenderActionPrepared(BondsAndParticles currentSystem,
                                                  BondsAndParticles currentSelection)
        {
            base.RenderActionPrepared(currentSystem, currentSelection);
            RenderAction(currentSystem);
        }

        public override void RenderActionInProgress(BondsAndParticles currentSystem,
                                                    BondsAndParticles currentSelection)
        {
            base.RenderActionPrepared(currentSystem, currentSelection);
            RenderAction(currentSystem);
        }

        public override void UpdateActionPrepared()
        {
            base.UpdateActionPrepared();
            UpdateAction();
        }

        public override void UpdateActionInProgress()
        {
            base.UpdateActionInProgress();

            if (type == BuildActionType.AddParticle)
            {
                newAtom.Pose = SimulationSpacePose.AsPointTransformationWithUnitScale();
                if (particlesToAlter.Count > 0)
                    guidelines =
                        GetGuidelinesAndDoSnap(particlesToAlter.OfType<Atom>(),
                                               SimulationSpacePose.Position);
            }
        }

        private void ClearUp()
        {
            ghostVisualisation.Destroy();
            guidelinesVisualiserValid.Destroy();
            guidelinesVisualiserInvalid.Destroy();
        }

        public override void OnActionCancelled()
        {
            base.OnActionCancelled();
            ClearUp();
        }

        public override void OnActionFinished()
        {
            base.OnActionFinished();
            PerformAction();
            ClearUp();
        }

        private void PerformAction()
        {
            var frame = CurrentSystem;
            switch (type)
            {
                case BuildActionType.AddParticle:
                    if (particlesToAlter.Count == 0)
                    {
                        frame.Particles.Add(newAtom);
                        frame.ReindexBondsAndParticles();
                    }
                    else
                    {
                        frame.Particles.Add(newAtom);
                        frame.Bonds.AddRange(
                            particlesToAlter.Select(p => new Bond(0, p, newAtom, 1)));
                        frame.ReindexBondsAndParticles();
                        frame.UpdateBondedParticlesForParticles();
                    }

                    Actions.SetSystemDirty();

                    break;
                case BuildActionType.ReplaceParticles:
                    foreach (var particle in particlesToAlter.OfType<Atom>())
                        particle.Element = element;
                    Actions.SetSystemDirty();
                    break;
            }
        }
    }
}