using System;
using System.Threading;
using System.Threading.Tasks;
using Narupa.Core.Async;
using NarupaBuilder;
using NarupaBuilder.OpenBabel;
using UnityEngine;
using UnityEngine.Assertions;
using Valve.VR;

namespace Narupa_Builder.Scripts.Actions
{
    /// <summary>
    /// Allows continuous minimisation using OpenBabel
    /// </summary>
    public class ContinuousMinimisation : MonoBehaviour
    {
        [SerializeField]
        private ActionsManager actionsManager;

        [SerializeField]
        private NarupaBuilder.NarupaBuilder builder;

        [SerializeField]
        private SteamVR_Action_Boolean minimiseAction;

        [SerializeField]
        private string forcefield = "mmff94s";

        protected bool IsMinimisationInProgress => minimisation != null;

        private void Awake()
        {
            Assert.IsNotNull(actionsManager);
            Assert.IsNotNull(minimiseAction);
            Assert.IsNotNull(builder);
        }

        private void OnEnable()
        {
            minimiseAction.onStateDown += MinimiseActionOnStateDown;
            minimiseAction.onStateUp += MinimiseActionOnStateUp;
        }

        private void OnDisable()
        {
            minimiseAction.onStateDown -= MinimiseActionOnStateDown;
            minimiseAction.onStateUp -= MinimiseActionOnStateUp;

            if (IsMinimisationInProgress)
                CancelMinimisation();
        }

        private Task<BondsAndParticles> minimisation;
        private CancellationTokenSource token = new CancellationTokenSource();

        private void CancelMinimisation()
        {
            if (IsMinimisationInProgress)
                token.Cancel();
        }

        private void MinimiseActionOnStateUp(SteamVR_Action_Boolean fromaction,
                                             SteamVR_Input_Sources fromsource)
        {
            CancelMinimisation();
        }

        private void MinimiseActionOnStateDown(SteamVR_Action_Boolean fromaction,
                                               SteamVR_Input_Sources fromsource)
        {
            if (minimisation == null)
            {
                StartMinimisationAsync().AwaitInBackground();
            }
        }

        private class MinimizeModal : Modal
        {
            public override bool HideMolecule => false;

            public MinimizeModal(ActionsManager aM) : base(aM)
            {
            }
        }

        /// <summary>
        /// Starts and awaits a minimisation Task.
        /// </summary>
        private async Task StartMinimisationAsync()
        {
            actionsManager.SetModal(new MinimizeModal(actionsManager));
            minimisation = OpenBabelMinimise.ContinuousMinimiseAsync(actionsManager.ActiveFrame,
                                                                     forcefield,
                                                                     UpdateSystem,
                                                                     token.Token);

            try
            {
                var result = await minimisation;
                FinishMinimisation(result);
            }
            catch (Exception e)
            {
                // Need to clearup after minimisation, even if there was an exception
                ClearupMinimisation();
                throw;
            }
        }

        private void ClearupMinimisation()
        {
            minimisation = null;
            token.Dispose();
            token = new CancellationTokenSource();
            actionsManager.DismissCurrentModal();
        }

        private void FinishMinimisation(BondsAndParticles result)
        {
            builder.EditableActiveFrame = result;
            builder.EditableActiveFrame.ReindexBondsAndParticles();
            builder.EditableActiveFrame.UpdateBondedParticlesForParticles();
            builder.SetSystemDirty();
            builder.SaveFrameDataToUndo();

            ClearupMinimisation();
        }

        private void UpdateSystem(BondsAndParticles obj)
        {
            builder.EditableActiveFrame = obj;
            builder.EditableActiveFrame.ReindexBondsAndParticles();
            builder.EditableActiveFrame.UpdateBondedParticlesForParticles();
        }
    }
}