// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;

namespace NarupaBuilder
{
    public abstract class Modal : IModal
    {
        public Modal(ActionsManager aM)
        {
            actionsManager = aM;
        }

        private ActionsManager actionsManager;

        public event Action ModalClosed;
        
        public abstract bool HideMolecule { get; }

        public virtual bool CanBeReplacedBy(IModal newModal)
        {
            return false;
        }

        public virtual void OnCloseModal()
        {
            ModalClosed?.Invoke();
        }

        public void Dismiss()
        {
            if (actionsManager.CurrentModal == this)
                actionsManager.DismissCurrentModal();
        }
    }
}