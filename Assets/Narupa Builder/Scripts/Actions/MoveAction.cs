﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using System.Linq;
using Narupa.Core.Math;

namespace NarupaBuilder
{
    public class MoveAction : BaseAction
    {
        private Transformation startPose;
        private float hitRangeMultiplier = 1f;

        public MoveAction(ActionsManager aM, VisualisationManager vM) : base(aM, vM)
        {
        }

        private new IEnumerable<Particle> GetHighlightedParticles()
        {
            if (Actions.HasSelection)
            {
                return Actions.Selection.Particles;
            }
            else
            {
                return base.GetHighlightedParticles();
            }
        }

        private IReadOnlyList<Particle> targetParticles = new List<Particle>();
        private IReadOnlyList<Particle> movedParticles = new List<Particle>();

        private IBuilderVisualiser targetVisualiser;

        public override void OnActionPrepared()
        {
            base.OnActionPrepared();
            targetVisualiser = Visualisations.MoveHighlight;
        }

        private void ClearUp()
        {
            targetVisualiser.Destroy();
        }

        public override void OnActionCancelled()
        {
            base.OnActionCancelled();
            ClearUp();
        }

        public override void OnActionFinished()
        {
            base.OnActionFinished();
            CurrentSystem.SetParticlePoseToMatchingParticlesPose(movedParticles);
            Actions.SetSystemDirty();
            ClearUp();
        }

        public override void OnActionStarted()
        {
            base.OnActionStarted();
            startPose = SimulationSpacePose;
            UpdateActionInProgress();
        }

        public override void UpdateActionPrepared()
        {
            base.UpdateActionPrepared();
            targetParticles = GetHighlightedParticles().ToList();
        }

        public override void UpdateActionInProgress()
        {
            base.UpdateActionInProgress();
            movedParticles = targetParticles.TransformParticles(startPose, SimulationSpacePose);
        }

        public override void RenderActionPrepared(BondsAndParticles currentSystem,
                                                  BondsAndParticles currentSelection)
        {
            base.RenderActionPrepared(currentSystem, currentSelection);
            targetVisualiser.SetBondsAndParticles(targetParticles,
                                                  currentSystem.Bonds.BondsContainingAnyParticles(
                                                      targetParticles));
        }

        public override void RenderActionInProgress(BondsAndParticles currentSystem,
                                                    BondsAndParticles currentSelection)
        {
            currentSystem.SetParticlePoseToMatchingParticlesPose(movedParticles);
            base.RenderActionInProgress(currentSystem, currentSelection);
            targetVisualiser.SetBondsAndParticles(movedParticles,
                                                  currentSystem.Bonds.BondsContainingAnyParticles(
                                                      movedParticles));
        }
    }
}