// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using System.Linq;
using Narupa.Core.Math;
using UnityEngine;

namespace NarupaBuilder
{
    public abstract class BaseAction : IAction
    {
        protected VisualisationManager Visualisations { get; }

        protected ActionsManager Actions { get; }

        protected BondsAndParticles CurrentSystem => Actions.ActiveFrame;

        public BaseAction(ActionsManager am, VisualisationManager vm)
        {
            Actions = am;
            Visualisations = vm;
        }

        public virtual void OnActionPrepared()
        {
        }

        public virtual void UpdateActionPrepared()
        {
        }

        public virtual void OnActionStarted()
        {
        }

        public virtual void UpdateActionInProgress()
        {
        }

        public virtual void OnActionFinished()
        {
        }

        public virtual void OnActionCancelled()
        {
        }

        public virtual void RenderActionPrepared(BondsAndParticles currentSystem,
                                                 BondsAndParticles currentSelection)
        {
        }

        public virtual void RenderActionInProgress(BondsAndParticles currentSystem,
                                                   BondsAndParticles currentSelection)
        {
        }

        protected Transformation SimulationSpacePose => Actions.SimulationSpacePose;

        protected Transformation SimulationSpacePoseUnitScale
        {
            get
            {
                var pose = SimulationSpacePose;
                return new Transformation(pose.Position, pose.Rotation, Vector3.one);
            }
        }
        
        protected Transformation WorldSpacePose => Actions.Pose;

        protected Sphere ToolSimulationSpaceSphere
            => Actions.CurrentToolSphere.InLocalSpace(Actions.StructureSpace);

        protected IOrderedEnumerable<Particle> GetHighlightedParticlesInOrder()
        {
            return HitChecks.SortedParticlesWhichOverlapSphere(
                ToolSimulationSpaceSphere,
                CurrentSystem.Particles);
        }

        protected IOrderedEnumerable<Particle> GetHighlightedParticlesInOrderForBuildReplace()
        {
            return HitChecks.SortedParticlesWhichOverlapSphere(
                Actions.CurrentToolSphere.InLocalSpaceGlobalScale(Actions.StructureSpace),
                CurrentSystem.Particles);
        }

        protected IEnumerable<Particle> GetHighlightedParticles()
        {
            return CurrentSystem.Particles.WhichOverlapSphere(ToolSimulationSpaceSphere);
        }

        protected List<Bond> GetHighlightedBondsInOrder()
        {
            return HitChecks.GetSortedBondsHit(CurrentSystem.Bonds, ToolSimulationSpaceSphere);
        }
    }
}