﻿// Copyright (c) Intangible Realities Laboratory. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;
using UnityEngine.Assertions;
using Valve.VR;

/// <summary>
/// Manager class which handles providing haptic feedback to the user.
/// </summary>
/// <remarks>
/// The various Vibrate___ functions can be linked to by any callback in the
/// Editor.
/// </remarks>
public class HapticsManager : MonoBehaviour
{
    [SerializeField]
    private SteamVR_Action_Vibration haptic;

    private void Awake()
    {
        Assert.IsNotNull(haptic);
    }

    /// <summary>
    /// Trigger the provided vibration on the left controller.
    /// </summary>
    public void VibrateLeftController(Vibration vibration)
    {
        haptic.Execute(0, vibration.Duration, vibration.Frequency, vibration.Amplitude,
                       SteamVR_Input_Sources.LeftHand);
    }

    /// <summary>
    /// Trigger the provided vibration on the right controller.
    /// </summary>
    public void VibrateRightController(Vibration vibration)
    {
        haptic.Execute(0, vibration.Duration, vibration.Frequency, vibration.Amplitude,
                       SteamVR_Input_Sources.RightHand);
    }

    /// <summary>
    /// Trigger the provided vibration on both controllers.
    /// </summary>
    public void VibrateBothControllers(Vibration vibration)
    {
        haptic.Execute(0, vibration.Duration, vibration.Frequency, vibration.Amplitude,
                       SteamVR_Input_Sources.Any);
    }
}