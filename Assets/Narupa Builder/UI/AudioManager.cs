﻿// Copyright (c) Intangible Realities Laboratory. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;

/// <summary>
/// Manager class which handles playing back audio.
/// </summary>
/// <remarks>
/// The <see cref="Play" />() function can be linked to by any callback in the
/// Editor.
/// </remarks>
public class AudioManager : MonoBehaviour
{
    [FormerlySerializedAs("audio")]
    [SerializeField]
    private AudioSource audioSource;

    private void Awake()
    {
        Assert.IsNotNull(audioSource);
    }

    /// <summary>
    /// Play the given audio clip.
    /// </summary>
    public void Play(AudioClip clip)
    {
        if (clip != null)
            audioSource.PlayOneShot(clip);
    }
}